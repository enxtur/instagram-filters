/**
 * Apply grayscale effect on the image
 */
export function grayscale() {
    /**
     * @param pixels - Image pixels
     * @returns Updated image pixels
     */
    return function(pixels: ImageData) {
        const { data } = pixels;

        for (let i = 0; i < data.length; i += 4) {
            const r = data[i];
            const g = data[i + 1];
            const b = data[i + 2];
            const avg = 0.2126 * r + 0.7152 * g + 0.0722 * b;
            data[i] = data[i + 1] = data[i + 2] = avg;
        }

        return pixels;
    };
}

/**
 * Apply sepia effect in the image
 *
 * @param adjustment - From 0 (unchanged) to 1 (sepia)
 */
export function sepia(adjustment: number) {
    /**
     * @param pixels - Image pixels
     * @returns Updated image pixels
     */
    return function(pixels: ImageData) {
        const { data } = pixels;

        for (let i = 0; i < data.length; i += 4) {
            const r = data[i];
            const g = data[i + 1];
            const b = data[i + 2];
            data[i] =
                r * (1 - 0.607 * adjustment) +
                g * 0.769 * adjustment +
                b * 0.189 * adjustment;
            data[i + 1] =
                r * 0.349 * adjustment +
                g * (1 - 0.314 * adjustment) +
                b * 0.168 * adjustment;
            data[i + 2] =
                r * 0.272 * adjustment +
                g * 0.534 * adjustment +
                b * (1 - 0.869 * adjustment);
        }

        return pixels;
    };
}

/**
 * Apply brightness effect on the image
 *
 * @param adjustment - From -1 (darker) to 1 (lighter). 0 is unchanged.
 */
export function brightness(adjustment: number) {
    /**
     * @param pixels - Image pixels
     * @returns Updated image pixels
     */
    return function(pixels: ImageData) {
        const { data } = pixels;
        adjustment = adjustment > 1 ? 1 : adjustment;
        adjustment = adjustment < -1 ? -1 : adjustment;
        adjustment = ~~(255 * adjustment);

        for (let i = 0; i < data.length; i += 4) {
            data[i] += adjustment;
            data[i + 1] += adjustment;
            data[i + 2] += adjustment;
        }

        return pixels;
    };
}

/**
 * Apply saturation effect on the image.
 *
 * @param adjustment - From -1 (desaturated) to positive number. 0 is unchanged.
 */
export function saturation(adjustment: number) {
    /**
     * @param pixels - Image pixels
     * @returns Updated image pixels
     */
    return function(pixels: ImageData) {
        const { data } = pixels;
        adjustment = adjustment < -1 ? -1 : adjustment;

        for (let i = 0; i < data.length; i += 4) {
            const r = data[i];
            const g = data[i + 1];
            const b = data[i + 2];
            const gray = 0.2989 * r + 0.587 * g + 0.114 * b; // weights from CCIR 601 spec
            data[i] = -gray * adjustment + data[i] * (1 + adjustment);
            data[i + 1] = -gray * adjustment + data[i + 1] * (1 + adjustment);
            data[i + 2] = -gray * adjustment + data[i + 2] * (1 + adjustment);
        }

        return pixels;
    };
}

/**
 * Apply contrast effect on the image
 *
 * @param adjustment - From -1 to 1
 */
export function contrast(adjustment: number) {
    /**
     * @param pixels - Image pixels
     * @returns Updated image pixels
     */
    return function(pixels: ImageData) {
        const { data } = pixels;
        adjustment *= 255;
        const factor = (259 * (adjustment + 255)) / (255 * (259 - adjustment));

        for (let i = 0; i < data.length; i += 4) {
            data[i] = factor * (data[i] - 128) + 128;
            data[i + 1] = factor * (data[i + 1] - 128) + 128;
            data[i + 2] = factor * (data[i + 2] - 128) + 128;
        }

        return pixels;
    };
}

/**
 * Apply color filter effect on the image. It adds a slight color overlay.
 *
 * @param rgbColor - [r, g, b, adjustment] tuple
 */
export function colorFilter([r, g, b, adjustment]: [
    number,
    number,
    number,
    number
]) {
    /**
     * @param pixels - Image pixels
     * @returns Updated image pixels
     */
    return function(pixels: ImageData) {
        const { data } = pixels;

        for (let i = 0; i < data.length; i += 4) {
            data[i] -= (data[i] - r) * adjustment;
            data[i + 1] -= (data[i + 1] - g) * adjustment;
            data[i + 2] -= (data[i + 2] - b) * adjustment;
        }

        return pixels;
    };
}

/**
 * Adjust RGB of the image
 *
 * @param adjustment - [r, g, b] tuple
 */
export function rgbAdjust([r, g, b]: [number, number, number]) {
    /**
     * @param pixels - Image pixels
     * @returns Updated image pixels
     */
    return function(pixels: ImageData) {
        const { data } = pixels;

        for (var i = 0; i < data.length; i += 4) {
            data[i] *= r;
            data[i + 1] *= g;
            data[i + 2] *= b;
        }

        return pixels;
    };
}
